<%--
  Created by IntelliJ IDEA.
  User: UdaraHerath
  Date: 4/6/2016
  Time: 10:09 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title></title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/font-awesome.css">
  <link rel="stylesheet" href="/css/bootstrap-social.css">
  <link rel="stylesheet" href="/css/Other.css">


  <!-- Generic page styles -->
  <link rel="stylesheet" href="/css/style.css">
  <!-- blueimp Gallery styles -->
  <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="/css/jquery.fileupload.css">
  <link rel="stylesheet" href="/css/jquery.fileupload-ui.css">
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript><link rel="stylesheet" href="/css/jquery.fileupload-noscript.css"></noscript>
  <noscript><link rel="stylesheet" href="/css/jquery.fileupload-ui-noscript.css"></noscript>

  <style>
    input.ng-invalid.ng-dirty{border:1px solid red;}
    input.ng-valid{border: 1px solid green;}

    select.ng-invalid.ng-dirty{border:1px solid red;}
    select.ng-valid{border: 1px solid green;}

    input.mname.ng-invalid.ng-dirty{border:1px solid lightgray;}
    input.mname.ng-valid{border: 1px solid lightgray;}
  </style>

</head>
  <body ng-app>

<div class="container" >
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">

      <form method="post" action="/quateServlet" enctype="multipart/form-data" novalidate="novalidate" name="infoForm" ng-sumbit="onSubmit(infoForm.$valid)">
      <div class="form-horizontal">
        <div class="panel-group" id="accordion">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="text-decoration: none">General Information &nbsp;<i class="fa fa-caret-down"></i> </a></h3>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body ">



            <div class="form-group">
              <label class="col-sm-4 control-label">Quatation Type</label>
              <div class="col-sm-8">

                <select class="form-control" ng-model="user.selectQuot" id="selectQuatation" name="selectQuatation" required>
                  <option value="" disabled selected>select your option</option>
                  <option value="3rdparty">3rd Party</option>
                  <option value="full">Full Insuranse</option>
                </select>

              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Full Name</label>
              <div class="col-sm-8">
                <div class="right-inner-addon">
                  <div ng-if="infoForm.inputFirstName.$error.text && infoForm.inputFirstName.$touched"  style="color: red"  >
                    <i class="fa fa-times "></i>
                  </div>
                  <div ng-if="infoForm.inputFirstName.$valid && infoForm.inputFirstName.$touched" style="color: green" >
                    <i class="fa fa-check"></i>
                  </div>
                <input type="text" class="form-control" id="inputFirstName" ng-model="user.fname" name="inputFirstName" placeholder="First Name" required/><br>

                  </div>

                  <div class="right-inner-addon">

                    <div ng-if="infoForm.inputMiddleName.$valid && infoForm.inputMiddleName.$touched" style="color: green" >
                      <i class="fa fa-check"></i>
                    </div>

                <input type="text" class="form-control mname" id="inputMiddleName" ng-model="user.name" name="inputMiddleName" placeholder="Middle Name" /><br>

                    </div>

                    <div class="right-inner-addon">
                      <div ng-if="infoForm.inputLastName.$error.text && infoForm.inputLastName.$touched"  style="color: red"  >
                        <i class="fa fa-times "></i>
                      </div>
                      <div ng-if="infoForm.inputLastName.$valid && infoForm.inputLastName.$touched" style="color: green" >
                        <i class="fa fa-check"></i>
                      </div>

                <input type="text" class="form-control" id="inputLastName" ng-model="user.lname" name="inputLastName" placeholder="Last Name" required/>
              </div>
                    </div>
              </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Address</label>
              <div class="col-sm-8">
                <textarea class="form-control" rows="4" id="inputAddress" name="inputAddress" placeholder="Address"></textarea>
              </div>
              </div>

            <div class="form-group" ng-app="myApp">
              <label class="col-sm-4 control-label">Phone</label>
              <div class="col-sm-8">
                <div class="right-inner-addon">
                  <div ng-if="infoForm.inputPhone.$error.number && infoForm.inputPhone.$error.maxlength && infoForm.inputPhone.$touched"  style="color: red"  >
                    <i class="fa fa-times "></i>
                  </div>
                  <div ng-if="infoForm.inputPhone.$valid && infoForm.inputPhone.$touched" style="color: green" >
                    <i class="fa fa-check"></i>
                  </div>
                <input size="10" type="number" class="form-control" id="inputPhone" ng-model="user.number" ng-minlength="10" ng-maxlength="10" name="inputPhone" placeholder="Telephone No:" required />

                  </div>
              </div>
            </div>

          <%--<div class="form-group">
            <div class="body" ng-app="myApp">
              <div class="wrapper" ng-controller="MyCtrl">
                <div class="info">Raw Value: {{phoneVal}}</div>
                <div class="info">Filtered Value: {{phoneVal | tel}}</div>

                <input class="input-phone" type='text' phone-input ng-model="phoneVal"/>
              </div>
            </div>
          </div>--%>








            <div class="form-group">

              <label class="col-sm-4 control-label">Email</label>

              <div class="col-sm-8 ">

                <%--<i class="fa fa-check "></i>--%>

                <div class="right-inner-addon">
                  <div ng-if="infoForm.inputEmail.$error.email && infoForm.inputEmail.$touched"  style="color: red"  >
                    <i class="fa fa-times "></i>
                  </div>
                  <div ng-if="infoForm.inputEmail.$valid && infoForm.inputEmail.$touched" style="color: green" >
                    <i class="fa fa-check"></i>
                  </div>
                  <input type="email" class="form-control" ng-model="user.email" name="inputEmail" placeholder="Email"  required />
                </div>

                </div>


            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Other Infomation</label>
              <div class="col-sm-8">
                <textarea class="form-control" rows="4" id="inputOtherInfo" name="inputOtherInfo" placeholder="Enter Other Infomations" ></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Preferred Insurance</label>
              <div class="col-sm-8">
                <div id="preferredInsurance">
                <div class="checkbox">
                  <label><input type="checkbox" value="srilanka" class="checkbox2" name="insuranceList">Sri Lanka Insurance</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="ceylinco"  class="checkbox2" name="insuranceList">Ceylinco VIP</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="mbsl"  class="checkbox2" name="insuranceList">MBSL</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="asianAlliance"  class="checkbox2" name="insuranceList">Asian Alliance</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="hnb"  class="checkbox2" name="insuranceList">HNB Insurance</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="all" class="check" id="selecctall2" name="insuranceList">Select All</label>
                </div>
                  </div>
              </div>
            </div>
          </div>
          </div>
        </div>


      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2" style="text-decoration: none">Vehical Information &nbsp;<i class="fa fa-caret-down"></i></a></h3>
        </div>
        <div id="collapse2" class="panel-collapse collapse out">
        <div class="panel-body">
          <div class="form-horizontal" >


            <div class="panel panel-primary">
              <div class="panel-heading">
                <p class="panel-title"><div class="radio ">
                  <label for="radio1"><input type="radio" name="optradio" value="radio1"><b>Upload a Photo of Vehicle Registration Card</b></label>
                </div></p>
              </div>
              <div class="panel-body">
                <div class="form-horizontal" >


                  <%--&lt;%&ndash;<script>
                    $(document).on('ready', function() {
                      $("#input-4").fileinput({showCaption: false});
                    });
                  </script>

                  <input id="images" name="images[]" type="file" multiple >
                  <hr>
                  <div id="images-to-upload">


                  </div>&ndash;%&gt;--%>




                  <!-- The file upload form used as target for the file upload widget -->
                  <div id="fileupload">
                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                   <%-- &lt;%&ndash;<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>&ndash;%&gt;--%>
                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                    <div class="row fileupload-buttonbar">
                      <div class="col-md-4">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                <%--<button class="btn btn-success fileinput-button "   >
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" class="radio1 " accept="image/jpeg" disabled="true" multiple>
                </button>--%>
                <input type="file" name="files[]" class="radio1 btn btn-success fileinput-button" accept="image/*" disabled="true" multiple />

                <button type="reset" class="btn btn-danger cancel radio1" disabled="true">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Remove</span>
                </button>


                     </div>

                    </div>

                    <!-- The table listing the files available for upload/download -->
                    <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                  </div>
                  <br>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Notes</h3>
                    </div>
                    <div class="panel-body">
                      <ul>
                        <li>The maximum images for uploads is <strong>3</strong></li>
                        <li>The maximum file size for uploads is <strong>999 KB</strong></li>
                        <li>Only image files (<strong>JPG, GIF, PNG</strong>) are allowed</li>
                        <li>You can <strong>drag &amp; drop</strong> files from your desktop on this webpage </li>


                      </ul>
                    </div>
                  </div>
                  <%--<script>
                    $('#images').on('change',function(e){
                      var files = e.target.files;

                      $.each(files,function(i,file){

                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function(e){

                          var template = '<form action="/upload">'+
                                  '<img src="'+e.target.result+'">'+
                                  '<button class="btn btn-sm btn-info">Upload</button>'+
                                  '<a href="#" class="btn btn-sm btn-danger">Remove</a>'+
                                  '</form>';

                          $('#images-to-upload').append(template);
                        }
                      });
                    });
                  </script>--%>




            </div>
            </div>
            </div>



            <div class="panel panel-primary">
              <div class="panel-heading">
                <p class="panel-title">
                <div class="radio">
                  <label for="radio2" class="col-sm-6"><input type="radio" name="optradio"  value="radio2"><b>Enter Information</b></label>
                </div></p>
              </div>

              <div class="panel-body">
                <div class="form-horizontal" >

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Vehicle Type</label>
                    <div class="col-sm-8">
                      <select class="form-control radio2" id="selectVehicle" name="selectVehicle"  disabled="true">
                        <option value="" disabled selected>Select your option(Required)</option>
                        <option value="Car">Car</option>
                        <option value="Van">Van</option>
                        <option value="Double cab">Double Cab</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Year of Manufactuer</label>
                    <div class="col-sm-8">
                      <div class="right-inner-addon">
                        <div ng-if="infoForm.inputYear.$error.number && infoForm.inputYear.$touched"  style="color: red"  >
                          <i class="fa fa-times "></i>
                        </div>
                        <div ng-if="infoForm.inputYear.$valid && infoForm.inputYear.$touched" style="color: green" >
                          <i class="fa fa-check"></i>
                        </div>
                      <input type="number" class="form-control radio2" id="inputYear" ng-model="user.year" name="inputYear" placeholder="Year"   disabled="true" required/>
                    </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Make</label>
                    <div class="col-sm-8">
                      <div class="right-inner-addon">
                        <div ng-if="infoForm.inputMake.$error.text && infoForm.inputMake.$touched"  style="color: red"  >
                          <i class="fa fa-times "></i>
                        </div>
                        <div ng-if="infoForm.inputMake.$valid && infoForm.inputMake.$touched" style="color: green" >
                          <i class="fa fa-check"></i>
                        </div>
                      <input type="text" class="form-control radio2" id="inputMake" ng-model="user.make" name="inputMake" placeholder="Make"    disabled="true" required/>
                    </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Model</label>
                    <div class="col-sm-8">
                      <div class="right-inner-addon">
                        <div ng-if="infoForm.inputModel.$error.text && infoForm.inputModel.$touched"  style="color: red"  >
                          <i class="fa fa-times "></i>
                        </div>
                        <div ng-if="infoForm.inputModel.$valid && infoForm.inputModel.$touched" style="color: green" >
                          <i class="fa fa-check"></i>
                        </div>
                      <input type="text" class="form-control radio2" id="inputModel" ng-model="user.model" name="inputModel" placeholder="Model"   disabled="true" required/>
                    </div>
                      </div>
                  </div>


                  <div class="form-group">
                    <label class="col-sm-4 control-label">Owners Name</label>
                    <div class="col-sm-8">
                      <div class="right-inner-addon">
                        <div ng-if="infoForm.inputOwner.$error.text && infoForm.inputOwner.$touched"  style="color: red"  >
                          <i class="fa fa-times "></i>
                        </div>
                        <div ng-if="infoForm.inputOwner.$valid && infoForm.inputOwner.$touched" style="color: green" >
                          <i class="fa fa-check"></i>
                        </div>
                      <input type="text" class="form-control radio2" id="inputOwner" ng-model="user.owner" name="inputOwner" placeholder="Owner"  disabled="true" required/>
                    </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Millage</label>
                    <div class="col-sm-8">

                      <div class="input-group">

                      <span class="input-group-addon">KM</span>
                        <div class="right-inner-addon">
                          <div ng-if="infoForm.inputMillage.$error.number && infoForm.inputMillage.$touched"  style="color: red"  >
                            <i class="fa fa-times "></i>
                          </div>
                          <div ng-if="infoForm.inputMillage.$valid && infoForm.inputMillage.$touched" style="color: green" >
                            <i class="fa fa-check"></i>
                          </div>
                      <input type="number" class="form-control radio2" id="inputMillage" ng-model="user.millage" name="inputMillage" placeholder="Millage" step="0.01"  disabled="true" required/>
                    </div>
                        </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-4 control-label">Market Value</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-addon">Rs.</span>
                        <div class="right-inner-addon">
                          <div ng-if="infoForm.inputValue.$error.number && infoForm.inputValue.$touched"  style="color: red"  >
                            <i class="fa fa-times "></i>
                          </div>
                          <div ng-if="infoForm.inputValue.$valid && infoForm.inputValue.$touched" style="color: green" >
                            <i class="fa fa-check"></i>
                          </div>
                        <input type="number" class="form-control radio2" placeholder="Market Value" ng-model="user.market" id="inputValue" name="inputValue" step="0.01" disabled="true" required/>
                      </div>
                        </div>


                    </div>
                  </div>



                  <div class="form-group">
                    <label class="col-sm-4 control-label">Current Insuer</label>
                    <div class="col-sm-8">
                      <select class="form-control radio2" id="selectCurrentInsuer" name="selectCurrentInsuer"  disabled="true">
                        <option value="srilanka">Srilnaka Insurance</option>
                        <option value="MBSL">MBSL</option>
                        <option value="Asian Alliance">Asian Alliance</option>
                        <option value="ceylinco">Ceylinco</option>
                        <option value="HNB Insurance">HNB Insurance</option>
                      </select>
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="col-sm-4 control-label">Other Infomation</label>
                    <div class="col-sm-8">
                <textarea class="form-control radio2" rows="4" id="inputOtherInfoVehicle" name="inputOtherInfoVehicle" placeholder="Enter Other Infomations"  disabled="true"></textarea>
                    </div>
                  </div>





                </div>
                  </div>
                </div>
              </div>
            </div>
        </div>





      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3" style="text-decoration: none">Insurance Option &nbsp;<i class="fa fa-caret-down"></i></a></h3>
        </div>
        <div id="collapse3" class="panel-collapse collapse out">
        <div class="panel-body">
          <div class="form-horizontal" >

            <div class="row">

              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-7">

                  <div class="checkbox chk-container">
                    <label><input type="checkbox" value="towing" class="checkbox1"  name="insuranceOption">Towing</label>
                  </div>
                  <div class="checkbox">
                    <label><input type="checkbox" value="riot" class="checkbox1"  name="insuranceOption">Riot Cover</label>
                  </div>
                  <div class="checkbox">
                    <label><input type="checkbox" value="flood" class="checkbox1"  name="insuranceOption">Flood Cover</label>
                  </div>
                  <div class="checkbox">
                    <label><input type="checkbox" value="natural"  class="checkbox1"  name="insuranceOption">Natural Disaster</label>
                  </div>

                  <div class="checkbox">
                    <label><input type="checkbox" value="all" id="selecctall" name="insuranceOption">Select All</label>
                  </div>
                </div>
                <div class="col-sm-4">

                </div>
              </div>
            </div>
          </div>




        </div>
        </div>
      </div>
        <br>

        <input type="submit" class="btn btn-primary text-right start" value="Send">


      </div>
      </div>
      </form>


        </div>

</div>
</div>








<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
<%--<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'> </script>--%>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>





    <div class="col-md-2"></div>
    
  </div>








<script>
  $(document).ready(function(){
    $("#selecctall").change(function(){
      $(".checkbox1").prop('checked', $(this).prop("checked"));
    });
  });

  $(document).ready(function(){
    $("#selecctall2").change(function(){
      $(".checkbox2").prop('checked', $(this).prop("checked"));
    });
  });
</script>

<script>
  var maxFiles = 1;
  var counter=0;
  $('#fileupload').fileupload({
    url: '/uploadUrl',
    add: function (e, data) {
      if(counter < maxFiles){
        counter++;
        alert("wrong");
      }else return false;

    }
  });
</script>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <%--<td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>--%>
        <%--&lt;%&ndash;<td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>&ndash;%&gt;--%>
        <td>
           <%--&lt;%&ndash; {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}&ndash;%&gt;--%>

            {% if (!i) { %}
                <button class="btn btn-danger cancel">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Remove</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<%--<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        &lt;%&ndash;<td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    &lt;%&ndash;<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>&ndash;%&gt;
                {% } %}
            </span>
        </td>&ndash;%&gt;
        &lt;%&ndash;<td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
       &lt;%&ndash;&ndash;%&gt; <td>
            &lt;%&ndash;{% if (file.deleteUrl) { %}&ndash;%&gt;
                <button class="btn btn-danger delete" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                &lt;%&ndash;<input type="checkbox" name="delete" value="1" class="toggle">&ndash;%&gt;
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>&ndash;%&gt;&ndash;%&gt;
    </tr>
{% } %}
</script>--%>

<script>
  $(document).ready(function(){
    $('input[type=radio][name=optradio]').click(function(){
      var related_class=$(this).val();
      $('.'+related_class).prop('disabled',false);

      $('input[type=radio][name=optradio]').not(':checked').each(function(){
        var other_class=$(this).val();
        $('.'+other_class).prop('disabled',true);
      });
    });
  });
</script>


<%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>--%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.js"></script>




<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/js/checkPhone.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="/js/main.js"></script>

<%--
<script src="/js/bootstrap-jsupload-js-jquery.fileupload-validate.js"></script>
--%>

</body>
</html>
