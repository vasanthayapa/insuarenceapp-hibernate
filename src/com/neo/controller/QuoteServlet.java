package com.neo.controller;

import com.neo.modal.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by MATRIX on 4/7/2016.
 */
@javax.servlet.annotation.WebServlet(name = "QuoteServlet")
@MultipartConfig(fileSizeThreshold=1024*1024*2,maxFileSize=1024*1024*10,maxRequestSize=1024*1024*25)
public class QuoteServlet extends HttpServlet {
    String referenceNumber="";
    private final String IMAGE_EXTENTION = ".jpg";
    private static final String DATA_DIRECTORY = "/uploads";
    private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 2;
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 25;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session=request.getSession();
        SystemIdGenarator systemIdGenarator=new SystemIdGenarator();//unique number generate class
        GeneralInformation generalInformation =new GeneralInformation();//insert customer details class
        VehicleInfo vehicleInfo=new VehicleInfo();// insert vehicle information(images/details) class
        Qutation qutation=new Qutation();//insert quotation and send email to both company and customer  class

        referenceNumber=systemIdGenarator.referenceNumberGenerator();//unique quotation number
        checkMutipart(request, referenceNumber);//check whether image or details
        String customerId=generalInformation.addCustomer(addToGeneralInfo(request));//inserting customer details
        Vehicle vehicle=addToVehicle(request);
        String PreferredInsurance= getPreferredInsurance(request);
        String InsuranceOptions=getInsuranceOptions(request);
      if((String) session.getAttribute("inputMake")!=null) {
            //insert vehicle information as details

          String vehicleId= vehicleInfo.addVehicle(vehicle,customerId);
           //send emaill to customer  and save quotation in database
         qutation.addQutation(PreferredInsurance, InsuranceOptions, referenceNumber, vehicleId, (String) session.getAttribute("inputEmail"), (String) session.getAttribute("inputFirstName"));
           // send email to insurance companies as details
          qutation.sendQutationsCompany(getPreferredInsurance(request), getInsuranceOptions(request), referenceNumber, vehicleId);
      }
       else{
       //insert vehicle information as images
          String[]path=getImagePath(request);

          String vehicleId = vehicleInfo.addVehicleImage(path, customerId);
            //send emaill to customer  and save quotation in database
            qutation.addQutation(getPreferredInsurance(request), getInsuranceOptions(request), referenceNumber, vehicleId, (String) session.getAttribute("inputEmail"),(String) session.getAttribute("inputFirstName"));
            // send email to insurance companies as images
           qutation.sendImageQutation(vehicleId, referenceNumber, getPreferredInsurance(request), getInsuranceOptions(request));
        }
        //display quotation number
        request.setAttribute("referenceNumber", referenceNumber);
        RequestDispatcher requestDispatcher=request.getRequestDispatcher("/view/thank.jsp");
        requestDispatcher.forward(request, response);
    }

    public Vehicle addToVehicle(HttpServletRequest request){
        Vehicle vehicle=new Vehicle();
        HttpSession session=request.getSession();

        vehicle.setVehicleType((String) session.getAttribute("selectVehicle"));
        vehicle.setManufactureDate(((String) session.getAttribute("inputYear")));
        vehicle.setManufactureCompany((String) session.getAttribute("inputMake"));
        vehicle.setVehicleModel((String) session.getAttribute("inputModel"));
        vehicle.setOwnersName((String) session.getAttribute("inputOwner"));
        if((String) session.getAttribute("inputMillage")==null || ((String) session.getAttribute("inputMillage")).isEmpty()){
            vehicle.setMileage(0.0);
        } else {
            vehicle.setMileage(Double.parseDouble((String) session.getAttribute("inputMillage")));
        }
        if ((String) session.getAttribute("inputValue")==null || ((String) session.getAttribute("inputValue")).isEmpty()){
            vehicle.setMarketValue(0.0);
        }else{
            vehicle.setMarketValue(Double.parseDouble((String) session.getAttribute("inputValue")));
        }
        vehicle.setCurrentInsure((String) session.getAttribute("selectCurrentInsuer"));
        vehicle.setOtherInfo((String) session.getAttribute("inputOtherInfoVehicle"));

        return vehicle;
    }

    //add general information
    public General addToGeneralInfo(HttpServletRequest request) {
        General general =new General();
        HttpSession session=request.getSession();



        general.setQuoteType((String) session.getAttribute("selectQuatation"));
        general.setCustomerFirstName((String) session.getAttribute("inputFirstName"));
        general.setCustomerMiddleName((String) session.getAttribute("inputMiddleName"));
        general.setCustomerLastName((String) session.getAttribute("inputLastName"));
        general.setAddress((String) session.getAttribute("inputAddress"));
        general.setPhone((String) session.getAttribute("inputPhone"));
        general.setEmail((String) session.getAttribute("inputEmail"));
        general.setOtherInfo((String) session.getAttribute("inputOtherInfo"));
        return general;
    }

    //return preferred insurance company list
    public String getPreferredInsurance(HttpServletRequest request){
        HttpSession session=request.getSession();

        return  (String)session.getAttribute("insuranceList");
    }

    //return insurance option list
    public String getInsuranceOptions(HttpServletRequest request){
        HttpSession session=request.getSession();

        return  (String)session.getAttribute("insuranceOption");
    }

    public String[] getImagePath(HttpServletRequest request){
        HttpSession session=request.getSession();
        String imagePath[]=new String[3];
        String image1=(String)session.getAttribute("image0");
        String image2=(String)session.getAttribute("image1");
        String image3=(String)session.getAttribute("image2");
        if(image1!=null && !image1.isEmpty()){
            imagePath[0]=image1;
        }else{
            imagePath[0]="";
        }if(image2!=null && !image2.isEmpty()){
            imagePath[1]=image2;
        }else{
            imagePath[1]="";
        }if(image3!=null && !image3.isEmpty()){
            imagePath[2]=image3;
        }else{
            imagePath[2]="";
        }
        return imagePath;
    }

    public void checkMutipart(HttpServletRequest request,String referenceNumber){
        HttpSession session=request.getSession();

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        String imagePath[]=new String[3];
        String companyList ="";
        String optionList="";
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(MAX_REQUEST_SIZE);

            try{
                List items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
                byte imageCount=0;
                while (iterator.hasNext()){
                    FileItem item=(FileItem)iterator.next();
                    if(item.isFormField()){
                        String name = item.getFieldName();
                        String value = item.getString();
                            if(item.getFieldName().equalsIgnoreCase("insuranceList")){
                                companyList=(String)session.getAttribute("insuranceList");
                                if(companyList==null){
                                    companyList=value;
                                }else if(companyList!=null){
                                    if(value.contains("all")){
                                        continue;
                                    }else {
                                        String temp = companyList.trim();
                                        companyList = temp + "," + value;
                                    }
                                }
                                session.setAttribute(name,companyList);
                            }else if(item.getFieldName().equalsIgnoreCase("insuranceOption")){
                                optionList=(String)session.getAttribute("insuranceOption");
                                if(optionList==null){
                                    optionList=value;
                                }else if(optionList!=null){
                                    if(value.contains("all")){
                                        continue;
                                    }else {
                                        String temp = optionList.trim();
                                        optionList = temp + "," + value;
                                    }
                                }
                                session.setAttribute(name,optionList);
                            }else{
                                session.setAttribute(name,value);
                            }
                        //addToGeneralInfo(name,value);

                    }else{
                        if(imageCount<3){
                            if(item.getSize()>0){
                                String fileName = item.getName();
                                fileName=referenceNumber+"_"+imageCount+IMAGE_EXTENTION;
                                String root = getServletContext().getRealPath("/");
                                File path = new File(root + DATA_DIRECTORY);
                                if (!path.exists()) {
                                    boolean status = path.mkdirs();
                                }
                                File uploadedFile = new File(path + "/" + fileName);
                                item.write(uploadedFile);
                                String finalPath=DATA_DIRECTORY+"/"+fileName;
                                imagePath[imageCount]=finalPath;
                                session.setAttribute("image"+imageCount,finalPath);
                                imageCount++;
                            }

                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
            doPost(request,response);
    }


}
