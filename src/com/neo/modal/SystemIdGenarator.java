package com.neo.modal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by wasantha yapa on 4/20/2016.
 */
public class SystemIdGenarator {

    Connection connection = ConnectionDB.connectDB();
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMdd");
    Random random =new Random();

    String number= Integer.toString(Math.abs(random.nextInt(1000)));
    Date date = new Date();
    String day=simpleDateFormat.format(date);

        public String idGenarator(String prefix) {
            String id=null;
            if (prefix == "vcl") {
                id="v"+day+number;
                String sql = "SELECT vehicle_info.vehicle_id,vehicle_image_info.vehicle_id FROM \"Insuarence\".vehicle_image_info,\"Insuarence\".vehicle_info";
                try {
                    PreparedStatement pst = connection.prepareStatement(sql);
                    ResultSet rs = pst.executeQuery();
                    while (rs.next()) {
                        String vid = rs.getString("vehicle_info.vehicle_id").trim();
                        String vidimg = rs.getString("vehicle_image_info.vehicle_id ").trim();
                        if (vid == id || vidimg== id) {
                            idGenarator(prefix);
                        }
                        else {
                            return id;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (prefix == "cstm") {
                 id="C"+day+number;
                String sql = "SELECT customer.customer_id FROM \"Insuarence\".customer";
                try {
                    PreparedStatement pst = connection.prepareStatement(sql);
                    ResultSet rs = pst.executeQuery();
                    while (rs.next()) {
                        String cid = rs.getString("customer_id").trim();

                        if (id == cid) {
                            idGenarator(prefix);
                        }
                        else {
                            return id;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }


            return id;
        }



        public String referenceNumberGenerator(){



            String reference_id=day+number;
            String sql = "SELECT qutation.reference_id FROM Insuarence.qutation";
            try {
                PreparedStatement pst = connection.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                while (rs.next()) {
                    String refId = rs.getString("qutation.reference_id").trim();
                    if (refId == reference_id) {
                        referenceNumberGenerator();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return reference_id;
        }
    }

