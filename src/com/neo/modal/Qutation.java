package com.neo.modal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by wasantha yapa on 4/13/2016.
 */
public class Qutation {
    Mailer mailer = new Mailer();
    Connection connection = ConnectionDB.connectDB();

    public String[] getPreferedCompanyList(String companyList) {
        List GetpreferedCompanyList = new ArrayList(Arrays.asList(companyList.split(",")));

        String[] GetpreferedCompanyListArray = new String[GetpreferedCompanyList.size()];
        GetpreferedCompanyListArray = (String[]) GetpreferedCompanyList.toArray(GetpreferedCompanyListArray);
        return GetpreferedCompanyListArray;
    }

    public String[] getInsuranceOptionsList(String optionlist) {
        List GetinsuranceOptionsList = new ArrayList(Arrays.asList(optionlist.split(",")));
        String[] GetinsuranceOptionsListArray = new String[GetinsuranceOptionsList.size()];
        GetinsuranceOptionsListArray = (String[]) GetinsuranceOptionsList.toArray(GetinsuranceOptionsListArray);
        return GetinsuranceOptionsListArray;
    }

    public void addQutation(String companyList, String optionlist, String referenceId, String vehicleId, String email, String fname) {

        String Company = "You have requested insurance quotation from following company(s)\n";
        String[] GetpreferedCompanyListArray = getPreferedCompanyList(companyList);
        for (String listArray : GetpreferedCompanyListArray) {
            Company = Company + "\n" + listArray;
        }
        Company = Company + "\nfor following insurance option\n";
        String[] GetinsuranceOptionsListArray = getInsuranceOptionsList(optionlist);
        for (String listArray : GetinsuranceOptionsListArray) {
            Company = Company + "\n" + listArray;
        }
        Company = Company + "\nThanks you";

        mailer.send(email, "Insurance Quotation Reference Number", "Hello " + fname + ",\nyour referenceNumber is " + referenceId + ".\n" + Company);
        sendQutations(companyList, optionlist, referenceId, vehicleId);

    }

    public void sendQutations(String companyList, String optionlist, String referenceId, String vehicleId) {

        String sql = "insert into \"Insuarence\".qutation(reference_id,vehicle_id,company,option)  values(?,?,?,?)";
        try {


            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            prepareStatement.setString(1, referenceId);
            prepareStatement.setString(2, vehicleId);
            prepareStatement.setString(3, companyList);
            prepareStatement.setString(4, optionlist);

            prepareStatement.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void sendImageQutation(String vid, String referenceId, String companyList, String optionlist) {
        String sql = "select * from \"Insuarence\".vehicle_image_info where vehicle_id='" + vid + "'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String[] image = new String[3];
                image[0] = resultSet.getString("vehicle_image1");
                image[1] = resultSet.getString("vehicle_image2");
                image[2] = resultSet.getString("vehicle_image3");
                String option = "";
                String[] GetinsuranceOptionsListArray = getInsuranceOptionsList(optionlist);
                for (String listArray : GetinsuranceOptionsListArray) {
                    option = option + "\n" + listArray;
                }

                String details = "Please give me a quotation for following details ";
                String optiondetails = "I accept following insurence options\n" + option + "\nMy quotation number is " + "'" + referenceId + "'\n\nThanks you";
                String[] GetpreferedCompanyListArray = getPreferedCompanyList(companyList);
                for (String listArray : GetpreferedCompanyListArray) {

                    String sql1 = "select company_email from \"Insuarence\".insurance_company where company_name='"+listArray+"'";
                    try {
                        PreparedStatement pst = connection.prepareStatement(sql1);
                       ResultSet rs = pst.executeQuery();
                        while (rs.next()) {
                            mailer.sendImage(rs.getString("company_email"), "Request Vehicle insurance Quotation", details, image, optiondetails);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendQutationsCompany(String companyList, String optionlist, String referenceId, String vehicleId) {

        Vehicle vehicle = new Vehicle();
        String sql = "select vehicle_type,manufacture_year,manufacture_company,modle,owner_name,milage,market_value from \"Insuarence\".vehicle_info where vehicle_id='" + vehicleId + "'";
        try {
            PreparedStatement pst = connection.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                vehicle.setVehicleType(rs.getString("vehicle_type"));
                vehicle.setManufactureDate(rs.getString("manufacture_year"));
                vehicle.setManufactureCompany(rs.getString("manufacture_company"));
                vehicle.setVehicleModel(rs.getString("modle"));
                vehicle.setOwnersName(rs.getString("owner_name"));
                vehicle.setMileage(rs.getDouble("milage"));
                vehicle.setMarketValue(rs.getDouble("market_value"));
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        String option = "";
        String[] GetinsuranceOptionsListArray = getInsuranceOptionsList(optionlist);
        for (String listArray : GetinsuranceOptionsListArray) {
            option = option + "\n" + listArray;
        }

        String details = "Please give me a quotation for following details \nVehicle Type\t" + vehicle.getVehicleType() + "\n Manufacture Year\t" + vehicle.getManufactureDate() + "\n Manufacture Company\t" + vehicle.getManufactureCompany() + "\n Vehicle Modle " + vehicle.getVehicleModel() + "\n Owner " + vehicle.getOwnersName() + "\n Market Value " + vehicle.getMarketValue() + "\n Milage " + vehicle.getMileage() + "\nI accept following insurence options\n" + option + "\nMy quotation number is " + "'" + referenceId + "'\n\nThanks you";
        String[] GetpreferedCompanyListArray = getPreferedCompanyList(companyList);
        for (String listArray : GetpreferedCompanyListArray) {

            String sql1 = "select company_email from \"Insuarence\".insurance_company where company_name='" + listArray + "'";
            try {
                PreparedStatement pst = connection.prepareStatement(sql1);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    mailer.send(rs.getString("company_email"), "Request Vehicle insuarence Qutaiopn", details);
                }
            } catch (Exception e) {
                e.printStackTrace();


            }
        }
    }
}





