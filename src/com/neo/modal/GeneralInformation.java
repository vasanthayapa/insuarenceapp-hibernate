package com.neo.modal;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * com.neo.modal.
 * Created by MATRIX on 4/7/2016.
 */
public class GeneralInformation {
    SystemIdGenarator systemIdGenarator = new SystemIdGenarator();

    public String addCustomer(General passGeneral){

        Connection connection = ConnectionDB.connectDB();
        String cutomer_id = systemIdGenarator.idGenarator("cstm");
        General general=passGeneral;
        general.setCustomerId(cutomer_id);
        String sql = "insert into \"Insuarence\".customer(customer_id,customer_first_name,customer_middle_name,customer_second_name,address,phone,email,other_info)  values(?,?,?,?,?,?,?,?)";
        try {


            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            prepareStatement.setString(1,general.getCustomerId());
            prepareStatement.setString(2, general.getCustomerFirstName());
            prepareStatement.setString(3, general.getCustomerMiddleName());
            prepareStatement.setString(4, general.getCustomerLastName());
            prepareStatement.setString(5, general.getAddress());
            prepareStatement.setInt(6, Integer.parseInt(general.getPhone()));
            prepareStatement.setString(7, general.getEmail());
            prepareStatement.setString(8, general.getOtherInfo());

            prepareStatement.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();

        }
        return general.getCustomerId();

    }
}
