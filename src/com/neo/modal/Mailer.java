package com.neo.modal;



import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * Created by wasantha yapa on 4/18/2016.
 */
public  class Mailer {
    public  void send(String to,String subject,String msg){

        final String user="kbandara9312@gmail.com";
        final String pass="wasantha123*";


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port","587");

        try {


        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        });


            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
            message.setSubject(subject);
            message.setText(msg);

            Transport.send(message);



        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public  void sendImage(String to,String subject,String msg,String[] image,String optionDetails){

        final String user="kbandara9312@gmail.com";
        final String pass="wasantha123*";


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port","587");

        try {


            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, pass);
                }
            });


            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(msg);
            MimeBodyPart msgbodypart;
            Multipart multipart =new MimeMultipart();
            msgbodypart =new MimeBodyPart();
            DataSource dataSource1=new FileDataSource(image[0].trim().toString());
            msgbodypart.setDataHandler(new DataHandler(dataSource1));
            //msgbodypart.setFileName("First.png");
            msgbodypart.setHeader(dataSource1.getName(), "image_id");
            multipart.addBodyPart(msgbodypart);
            message.setContent(multipart);


            msgbodypart =new MimeBodyPart();
            DataSource dataSource2=new FileDataSource(image[1].trim().toString());
            msgbodypart.setDataHandler(new DataHandler(dataSource2));
            msgbodypart.setFileName("Second.jpg");
            multipart.addBodyPart(msgbodypart);
            message.setContent(multipart);

            msgbodypart =new MimeBodyPart();
            DataSource dataSource3=new FileDataSource(image[2].trim().toString());
            msgbodypart.setDataHandler(new DataHandler(dataSource3));
            msgbodypart.setFileName("Third.jpg");
            multipart.addBodyPart(msgbodypart);
            message.setContent(multipart);
            message.setText(optionDetails);
            Transport.send(message);



        }
          /*  // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(user));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject: header field
            message.setSubject("Testing Subject");

            // This mail has 2 part, the BODY and the embedded image
            MimeMultipart multipart = new MimeMultipart("related");

            // first part (the html)
            BodyPart messageBodyPart = new MimeBodyPart();
            String htmlText = "<H1>Hello</H1><img src=\"cid:image\">";
            messageBodyPart.setContent(htmlText, "text/html");
            // add it
            multipart.addBodyPart(messageBodyPart);

            // second part (the image)
            messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(image[0]);

            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<image>");

            // add image to the multipart
            multipart.addBodyPart(messageBodyPart);

            // put everything together
            message.setContent(multipart);
            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");}*/
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
