package com.neo.modal;

/**
 * com.neo.modal.
 * Created by MATRIX on 4/8/2016.
 */
public class Insurance {
    private String preferredInsuranceList;
    private String insuranceOptionsList;

    public String getPreferredInsuranceList() {
        return preferredInsuranceList;
    }

    public void setPreferredInsuranceList(String preferredInsuranceList) {
        this.preferredInsuranceList = preferredInsuranceList;
    }

    public String getInsuranceOptionsList() {
        return insuranceOptionsList;
    }

    public void setInsuranceOptionsList(String insuranceOptionsList) {
        this.insuranceOptionsList = insuranceOptionsList;
    }
}
