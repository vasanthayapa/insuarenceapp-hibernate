package com.neo.modal;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * com.neo.modal.
 * Created by MATRIX on 4/7/2016.
 */
public class VehicleInfo {

   SystemIdGenarator systemIdGenarator = new SystemIdGenarator();
   Connection connection=ConnectionDB.connectDB();
   public String addVehicle(Vehicle passVehicle,String cid) {


      String vehicle_id = systemIdGenarator.idGenarator("vcl");
      Vehicle vehicle = passVehicle;
      vehicle.setVehicleId(vehicle_id);

      String sql = "INSERT INTO \"Insuarence\".vehicle_info(vehicle_id,vehicle_type,manufacture_year,manufacture_company,modle,owner_name,milage,market_value,current_insure,other_info,customer_id) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
      try {


         PreparedStatement prepareStatement = connection.prepareStatement(sql);
         prepareStatement.setString(1, vehicle.getVehicleId());
         prepareStatement.setString(2, vehicle.getVehicleType());
         prepareStatement.setString(3, vehicle.getManufactureDate());
         prepareStatement.setString(4, vehicle.getManufactureCompany());
         prepareStatement.setString(5, vehicle.getVehicleModel());
         prepareStatement.setString(6, vehicle.getOwnersName());
         prepareStatement.setDouble(7, vehicle.getMileage());
         prepareStatement.setDouble(8, vehicle.getMarketValue());
         prepareStatement.setString(9, vehicle.getCurrentInsure());
         prepareStatement.setString(10, vehicle.getOtherInfo());
         prepareStatement.setString(11,cid);
         prepareStatement.executeUpdate();


      } catch (Exception e) {
            e.printStackTrace();
      }

      return  vehicle_id;
   }
   public String addVehicleImage(String[]vehicleImage,String cid){


      String vehicle_id = systemIdGenarator.idGenarator("vcl");
      String sql="Insert into \"Insuarence\".vehicle_image_info(vehicle_id,customer_id,vehicle_image1,vehicle_image2,vehicle_image3) values(?,?,?,?,?)";
      try{
         PreparedStatement prepareStatement = connection.prepareStatement(sql);
         prepareStatement.setString(1,vehicle_id);
         prepareStatement.setString(2, cid);
         prepareStatement.setString(3, vehicleImage[0]);
         prepareStatement.setString(4,vehicleImage[1]);
         prepareStatement.setString(5,vehicleImage[2]);
         prepareStatement.executeUpdate();
      }catch (Exception e){
         e.printStackTrace();
      }

return vehicle_id;
   }
}
