package com.neo.modal;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by wasantha yapa on 4/7/2016.
 */
public class ConnectionDB {
    public static Connection connectDB(){

        try {

            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/abcinsuarence", "postgres", "1234");
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
