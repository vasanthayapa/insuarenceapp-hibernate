package com.neo.modal;

/**
 * Created by MATRIX on 4/7/2016.
 */
public class Vehicle {
    private String vehicleId;
    private String vehicleType;
    private String manufactureDate;
    private String manufactureCompany;
    private String vehicleModel;
    private String ownersName;
    private Double mileage;
    private Double marketValue;
    private String otherInfo;
    private String currentInsure;
    private String image_1;
   private String image_2;
   private String image_3;



    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getManufactureCompany() {
        return manufactureCompany;
    }

    public void setManufactureCompany(String manufactureCompany) {
        this.manufactureCompany = manufactureCompany;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getOwnersName() {
        return ownersName;
    }

    public void setOwnersName(String ownersName) {
        this.ownersName = ownersName;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Double getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(Double marketValue) {
        this.marketValue = marketValue;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getCurrentInsure() {
        return currentInsure;
    }

    public void setCurrentInsure(String currentInsure) {
        this.currentInsure = currentInsure;
    }

//    public String getImage_1() {
//        return image_1;
//    }
//
//    public void setImage_1(String image_1) {
//        this.image_1 = image_1;
//    }
//
//    public String getImage_2() {
//        return image_2;
//    }
//
//    public void setImage_2(String image_2) {
//        this.image_2 = image_2;
//    }
//
//    public String getImage_3() {
//        return image_3;
//    }
//
//    public void setImage_3(String image_3) {
//        this.image_3 = image_3;
//    }
}
