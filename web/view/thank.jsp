<%--
  Created by IntelliJ IDEA.
  User: UdaraHerath
  Date: 4/5/2016
  Time: 2:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <title>ABC Insurance</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <link rel="stylesheet" href="/css/font-awesome.css">
  <link rel="stylesheet" href="/css/bootstrap-social.css">
  <link rel="stylesheet" href="/css/Other.css">
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top" id="my-nav-bar">
  <div class="container-fluid row-colour-bg" >
    <div class="navbar-header">
      <button type="button" class="navbar-toggle row-colour-to" data-toggle="collapse" data-target="#navbar-collpase">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="#" class="navbar-brand"><img  src="/img/logo.png"></a>

    </div>


    <div class="collapse navbar-collapse"   id="navbar-collpase">
      <div style="margin-right: 2px" >
        <a href="#" class="btn navbar-btn navbar-right  btn-social-icon btn-primary margin-left"><span class="glyphicon glyphicon-envelope"></span></a>
        <a href="#" class="btn navbar-btn navbar-right btn-social-icon btn-google margin-left"><span class="fa fa-google-plus"></span></a>
        <a href="#" class="btn navbar-btn navbar-right btn-social-icon btn-facebook margin-left"><span class="fa fa-facebook"></span></a>

      </div>

      <ul class="nav navbar-nav">
        <li><a href="#" style="color: whitesmoke">Home</a>
        <li><a href="#" style="color: whitesmoke">About us</a>
        <li><a href="#" style="color: whitesmoke">Contact us</a>
      </ul>
    </div>

  </div>
</nav>

<div class="jumbotron row-colour" style="margin-bottom: -10px !important; padding-bottom: -10px !important; margin-top: -10px !important; padding-top: -10px !important;">
  <div class="container text-center" style="padding-top: 15px">
    <h3 style="color: ghostwhite">Thank you for Submitting your Request. We will get back to you as soon as possible.</h3>
     <h3 style="color: red"> Your refference number is
    <span> #<c:out value="${referenceNumber}"/></span></h3>
    <span><h3 style="color: ghostwhite">Check your inbox</h3></span>
    <br>
    <br>

   <%-- <span>
    <c: forEach var="row" items="${rows}">
      <tr>
        <td><c: out value="${row.referenceNumber}"/></td>

      </tr>
    </c:forEach>
      </span>--%>

    <div class="row">
      <div class="col-sm-5 col-md-4"></div>
      <div class="col-sm-2 col-md-4">

      </div>
      <div class="col-sm-5 col-md-4"></div>
    </div>
  </div>

  <div class="container text-center">
    <img class="img_tha" src="/img/thank-you.png">
  </div>


</div>
<div style="height: 5px; width: 100%;  margin: 0; padding: 0; border: 0; display: block;">

<div class="well">
  <div class="container-fluid text-center " >
    <b> &copy;2016 Neolith Technologies<br>
      www.neolithtech.com</b>
  </div>
</div>
</div>


<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
