angular.module('ui.bootstrap.demo', ['ngAnimate', 'ui.bootstrap']);
angular.module('ui.bootstrap.demo').controller('DatepickerDemoCtrl', function ($scope) {

	$scope.clear = function () {
		$scope.dt = null;
	};

	$scope.open = function($event) {
		$scope.status.opened = true;
	};

	$scope.setDate = function(year) {
		$scope.dt = new Date(year);
	};

	$scope.dateOptions = {
		formatYear: 'yyyy',
		startingDay: 1,
		minMode: 'year'
	};

	$scope.formats = ['yyyy'];
	$scope.format = $scope.formats[0];

	$scope.status = {
		opened: false
	};
});